#
# See slapd.conf(5) for details on configuration options.
# This file should NOT be world readable.
#
include		/etc/ldap/schema/core.schema
include		/etc/ldap/schema/cosine.schema
include		/etc/ldap/schema/inetorgperson.schema
include         /etc/ldap/schema/ppolicy.schema

include         /etc/ldap/kde-schema/rfc2307bis.schema
include         /etc/ldap/kde-schema/kde.schema
include         /etc/ldap/kde-schema/openssh.schema

# Define global ACLs to disable default read access.

# Do not enable referrals until AFTER you have a working directory
# service AND an understanding of referrals.
#referral	ldap://root.openldap.org

pidfile		/var/run/slapd/slapd.pid
argsfile	/var/run/slapd/slapd.args

modulepath      /usr/lib/ldap/
moduleload      ppolicy.la
moduleload      syncprov.la
moduleload      back_mdb.la

# Load dynamic backend modules:
# modulepath	/usr/lib/openldap/modules
# moduleload	back_bdb.la
# moduleload	back_hdb.la
# moduleload	back_ldap.la

# Sample security restrictions
#	Require integrity protection (prevent hijacking)
#	Require 112-bit (3DES or better) encryption for updates
#	Require 63-bit encryption for simple bind
# security ssf=1 update_ssf=112 simple_bind=64

#TLSCipherSuite HIGH:MEDIUM:-SSLv2
#TLSCACertificateFile /home/gosaadmin/ldapssl/ca.crt
#TLSCertificateFile /home/gosaadmin/ldapssl/server.crt
#TLSCertificateKeyFile /home/gosaadmin/ldapssl/server.key

# Sample access control policy:
#       Root DSE: allow anyone to read it
#       Subschema (sub)entry DSE: allow anyone to read it
#       Other DSEs:
#               Allow self write access to user password
#               Allow anonymous users to authenticate
#               Allow read access to everything else
#       Directives needed to implement policy:
access to dn.base=""
        by * read

access to dn.base="cn=Subschema"
        by * read

# Solena ACL Set

# Allow sysadmins and the system to administer our organisational structure (required for user and group creation as well)
# Everyone else is permitted to read the entire organisation structure
access to filter=(|(objectClass=dcObject)(objectClass=organizationalUnit))
   by group.exact="cn=sysadmins,ou=groups,dc=kde,dc=org" write
   by dn.exact="cn=solena-service,dc=kde,dc=org" write
   by users read

# Allow the ev board to update the membership of the ev-members group
access to dn.exact="cn=ev-members,ou=groups,dc=kde,dc=org" attrs=description,member,memberUid
   by group.exact="cn=ev-board,ou=groups,dc=kde,dc=org" write
   by * break

# Allow the ev board to update the membership of the ev-board group
access to dn.exact="cn=ev-board,ou=groups,dc=kde,dc=org" attrs=description,member,memberUid
   by group.exact="cn=ev-board,ou=groups,dc=kde,dc=org" write
   by * break

# Allow everyone to see the membership of all groups
# Allow sysadmins and the system to administer all groups
access to dn.children="ou=groups,dc=kde,dc=org"
   by group.exact="cn=sysadmins,ou=groups,dc=kde,dc=org" write
   by dn.exact="cn=solena-service,dc=kde,dc=org" write
   by users read

# Allow people to change their passwords
# Sysadmins and the system are also allowed to change passwords, for the purpose of reseting them
access to filter=(objectClass=inetOrgPerson) attrs=userPassword
   by self +w
   by group.exact="cn=sysadmins,ou=groups,dc=kde,dc=org" +w
   by dn.exact="cn=solena-service,dc=kde,dc=org" +w
   by * auth

# Allow sysadmins to lock and unlock accounts
access to filter=(objectClass=kdeAccount) attrs=pwdAccountLockedTime,pwdChangedTime
   by group.exact="cn=sysadmins,ou=groups,dc=kde,dc=org" write
   by dn.exact="uid=service-phabricator,ou=people,ou=services,dc=kde,dc=org" read

# The system needs to be able to register people and confirm developer applications
access to filter=(objectClass=kdeAccount) attrs=entry,objectClass,groupMember,uid,givenName,sn,cn,mail,secondaryMail,sshPublicKey,twoFactorAuthentication
   by dn.exact="cn=solena-service,dc=kde,dc=org" write
   by * break

# Allow people to modify their own profiles
access to filter=(objectClass=kdeAccount) attrs=entry,uid
   by group.exact="cn=sysadmins,ou=groups,dc=kde,dc=org" write
   by self read
   by * break
   
access to filter=(objectClass=kdeAccount) attrs=objectClass,groupMember,givenName,sn,cn,dateOfBirth,gender,timezone,timezoneName,homePostalAddress,homePhone,labeledURI,ircNick,jabberID,mail,secondaryMail,jpegPhoto,twoFactorAuthentication
   by group.exact="cn=sysadmins,ou=groups,dc=kde,dc=org" write
   by self write
   by * break

# Allow developers to update their SSH keys
access to filter=(|(groupMember=developers)(groupMember=disabled-developers)) attrs=sshPublicKey
   by group.exact="cn=sysadmins,ou=groups,dc=kde,dc=org" write
   by self write
   by * break

# Allow ev members to change their contact address for ev concerns, and other members to see that address
access to filter=(objectClass=kdeAccount) attrs=evMail
   by self write
   by group.exact="cn=sysadmins,ou=groups,dc=kde,dc=org" write
   by group.exact="cn=ev-board,ou=groups,dc=kde,dc=org" write
   by group.exact="cn=ev-members,ou=groups,dc=kde,dc=org" read
   by * break
   
# Allow the ev board to change the membership of the ev and the status of members
access to filter=(objectClass=kdeAccount) attrs=objectClass,groupMember,memberStatus
   by group.exact="cn=sysadmins,ou=groups,dc=kde,dc=org" write
   by group.exact="cn=ev-board,ou=groups,dc=kde,dc=org" write
   by dn.exact="cn=solena-service,dc=kde,dc=org" read
   by * break

# Allow ev members to view more attributes of other members
access to filter=(groupMember=ev-members) attrs=entry,objectClass,groupMember,uid,memberStatus,givenName,sn,cn,dateOfBirth,gender,timezone,timezoneName,homePostalAddress,homePhone,labeledURI,ircNick,jabberID,mail,secondaryMail,jpegPhoto
   by group.exact="cn=ev-members,ou=groups,dc=kde,dc=org" read
   by * break

# Allow the script service account extra special permissions to read all entry, entryUUID and entryCSN values, so replication can operate smoothly
access to attrs=entry,entryUUID,entryCSN
   by dn.exact="uid=service-scripts,ou=people,ou=services,dc=kde,dc=org" read
   by * break

# Allow service accounts to view basic details on everyone so they can be logged in and have application profiles populated
# Allow the ev board to view basic details on everyone so it is possible to add members to the ev-members group
access to filter=(objectClass=kdeAccount) attrs=entry,objectClass,groupMember,uid,givenName,sn,cn,mail,secondaryMail,sshPublicKey,jpegPhoto,memberStatus,timezoneName,entryUUID
   by group.exact="cn=ev-board,ou=groups,dc=kde,dc=org" read
   by group.exact="cn=akademy-team,ou=groups,dc=kde,dc=org" read
   by group.exact="cn=identity-support,ou=groups,dc=kde,dc=org" read
   by dn.children="ou=services,dc=kde,dc=org" read
   by * break

# Allow basic attributes of developers to be accessible to everyone
access to filter=(|(groupMember=developers)(groupMember=disabled-developers)) attrs=entry,objectClass,groupMember,uid,givenName,sn,cn,mail
   by users read
   by * break

# Allow developers to view more attributes of other developers
access to filter=(|(groupMember=developers)(groupMember=disabled-developers)) attrs=entry,objectClass,groupMember,uid,givenName,sn,cn,gender,timezone,timezoneName,labeledURI,mail,secondaryMail,jabberID,ircNick,jpegPhoto
   by group.exact="cn=developers,ou=groups,dc=kde,dc=org" read
   by group.exact="cn=disabled-developers,ou=groups,dc=kde,dc=org" read
   by * break

#######################################################################
# BDB database definitions
#######################################################################

database	mdb
suffix		"dc=kde,dc=org"
checkpoint      0 15
dbnosync
maxsize         85899345920
rootdn		"cn=Sysadmin,dc=kde,dc=org"
require authc

# Cleartext passwords, especially for the rootdn, should
# be avoid.  See slappasswd(8) and slapd.conf(5) for details.
# Use of strong authentication encouraged.
rootpw		{{ldap_root_password_hash}}

overlay ppolicy
ppolicy_default        "cn=passwordpolicy,dc=kde,dc=org"
ppolicy_hash_cleartext
ppolicy_use_lockout

overlay syncprov
syncprov-checkpoint 100 10
syncprov-sessionlog 100

authz-policy both
authz-regexp uid=system-service,cn=digest-md5,cn=auth cn=system-service,dc=kde,dc=org
authz-regexp uid=([^,]*),cn=digest-md5,cn=auth uid=$1,ou=people,dc=kde,dc=org

# Needed to display a full list of users...
sizelimit       unlimited

# No need to log
{# TODO make this configurable for prod vs dev #}
loglevel 0

# The database directory MUST exist prior to running slapd AND 
# should only be accessible by the slapd and slap tools.
# Mode 700 recommended.
directory	/var/lib/ldap

# Indices to maintain
index   uid,givenName,sn,cn                             pres,eq,sub
index   mail,secondaryMail,jabberID,ircNick             pres,eq,sub
index   member,memberUid,groupMember                    pres,eq
index   objectClass,entryCSN,entryUUID                  pres,eq
index   memberStatus                                    pres,eq
index   uidNumber,gidNumber                             pres,eq
