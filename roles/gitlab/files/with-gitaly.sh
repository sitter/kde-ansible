#!/bin/bash

# This is a hack to start a temporary instance of Gitaly, run another
# command, and kill Gitaly. It's used for initial deployment of GitLab,
# specifically the database initialization step.

set -x
set -e

app_root="/home/git/gitlab"
pid_path="$app_root/tmp/pids"
gitaly_pid_path="$pid_path/gitaly.pid"
gitaly_log="$app_root/log/gitaly.log"

gitaly_dir=$(realpath $app_root/../gitaly)

$app_root/bin/daemon_with_pidfile $gitaly_pid_path $gitaly_dir/_build/bin/gitaly $gitaly_dir/config.toml &>> $gitaly_log
trap 'kill $(cat $gitaly_pid_path); rm "$gitaly_pid_path"' 0

"$@"; exitcode=$?

exit $exitcode
