-- MySQL dump 10.13  Distrib 5.7.33, for Linux (x86_64)
--
-- Host: localhost    Database: l10n
-- ------------------------------------------------------
-- Server version	5.7.33-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `l10n_dictionary`
--

DROP TABLE IF EXISTS `l10n_dictionary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `l10n_dictionary` (
  `mode` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revision` varchar(16) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `teamcode` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `package` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `filename` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `msgid` varchar(6000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `msgstr` varchar(6000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `msgctxt` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  KEY `teamcode` (`teamcode`),
  KEY `filename` (`filename`),
  KEY `msgstr` (`msgstr`(250)),
  KEY `msgid` (`msgid`(250))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `l10n_doccfgessential`
--

DROP TABLE IF EXISTS `l10n_doccfgessential`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `l10n_doccfgessential` (
  `type` enum('p','f') NOT NULL DEFAULT 'f',
  `filename` char(100) NOT NULL DEFAULT '',
  `percent` int(2) NOT NULL DEFAULT '0',
  `rev` char(25) DEFAULT NULL,
  PRIMARY KEY (`type`,`filename`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `l10n_doccfgpackage`
--

DROP TABLE IF EXISTS `l10n_doccfgpackage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `l10n_doccfgpackage` (
  `package` char(40) NOT NULL DEFAULT '',
  PRIMARY KEY (`package`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `l10n_docessential`
--

DROP TABLE IF EXISTS `l10n_docessential`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `l10n_docessential` (
  `rev` char(25) NOT NULL DEFAULT '',
  `sdate` date NOT NULL,
  `teamcode` char(11) NOT NULL DEFAULT '',
  `filename` char(100) NOT NULL DEFAULT '',
  `translated` int(4) NOT NULL DEFAULT '0',
  `total` int(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`rev`,`sdate`,`teamcode`,`filename`),
  KEY `sdate` (`sdate`),
  KEY `teamcode` (`teamcode`),
  KEY `rev` (`rev`),
  KEY `filename` (`filename`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `l10n_docstats`
--

DROP TABLE IF EXISTS `l10n_docstats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `l10n_docstats` (
  `rev` char(25) NOT NULL DEFAULT '',
  `teamcode` char(11) NOT NULL DEFAULT '',
  `package` char(40) NOT NULL DEFAULT '',
  `filename` char(100) NOT NULL DEFAULT '',
  `translated` int(4) NOT NULL DEFAULT '0',
  `fuzzy` int(4) NOT NULL DEFAULT '0',
  `untranslated` int(4) NOT NULL DEFAULT '0',
  `error` int(4) NOT NULL DEFAULT '0',
  `have_po` tinyint(1) NOT NULL DEFAULT '0',
  `have_pot` tinyint(1) NOT NULL DEFAULT '0',
  `sdate` date NOT NULL,
  PRIMARY KEY (`rev`,`teamcode`,`package`,`filename`,`sdate`),
  KEY `sdate_rev_teamcode` (`sdate`,`rev`,`teamcode`),
  KEY `rev` (`rev`),
  KEY `sdate` (`sdate`),
  KEY `filename` (`filename`),
  KEY `package` (`package`),
  KEY `teamcode` (`teamcode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `l10n_guicfgessential`
--

DROP TABLE IF EXISTS `l10n_guicfgessential`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `l10n_guicfgessential` (
  `type` enum('p','f') NOT NULL DEFAULT 'f',
  `filename` char(100) NOT NULL DEFAULT '',
  `percent` int(2) NOT NULL DEFAULT '0',
  `rev` char(25) NOT NULL DEFAULT '',
  PRIMARY KEY (`filename`,`rev`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `l10n_guicfgpackage`
--

DROP TABLE IF EXISTS `l10n_guicfgpackage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `l10n_guicfgpackage` (
  `package` char(40) NOT NULL DEFAULT '',
  PRIMARY KEY (`package`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `l10n_guiessential`
--

DROP TABLE IF EXISTS `l10n_guiessential`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `l10n_guiessential` (
  `rev` char(25) NOT NULL DEFAULT '',
  `sdate` date NOT NULL,
  `teamcode` char(11) NOT NULL DEFAULT '',
  `filename` char(100) NOT NULL DEFAULT '',
  `translated` int(4) NOT NULL DEFAULT '0',
  `total` int(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`rev`,`sdate`,`teamcode`,`filename`),
  KEY `sdate` (`sdate`),
  KEY `teamcode` (`teamcode`),
  KEY `rev` (`rev`),
  KEY `filename` (`filename`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `l10n_guistats`
--

DROP TABLE IF EXISTS `l10n_guistats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `l10n_guistats` (
  `rev` char(25) NOT NULL DEFAULT '',
  `teamcode` char(11) NOT NULL DEFAULT '',
  `package` char(40) NOT NULL DEFAULT '',
  `filename` char(100) NOT NULL DEFAULT '',
  `translated` int(4) NOT NULL DEFAULT '0',
  `fuzzy` int(4) NOT NULL DEFAULT '0',
  `untranslated` int(4) NOT NULL DEFAULT '0',
  `error` int(4) NOT NULL DEFAULT '0',
  `have_po` tinyint(1) NOT NULL DEFAULT '0',
  `have_pot` tinyint(1) NOT NULL DEFAULT '0',
  `sdate` date NOT NULL,
  PRIMARY KEY (`rev`,`teamcode`,`package`,`filename`,`sdate`),
  KEY `teamcode` (`teamcode`),
  KEY `package` (`package`),
  KEY `rev` (`rev`),
  KEY `sdate` (`sdate`),
  KEY `filename` (`filename`),
  KEY `sdate_2` (`sdate`,`rev`),
  KEY `sdate_3` (`sdate`),
  KEY `teamcode_2` (`teamcode`,`rev`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `l10n_revisions`
--

DROP TABLE IF EXISTS `l10n_revisions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `l10n_revisions` (
  `rev` char(25) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `main` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`rev`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `l10n_teams`
--

DROP TABLE IF EXISTS `l10n_teams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `l10n_teams` (
  `teamcode` char(11) NOT NULL DEFAULT '',
  `teamname` char(80) NOT NULL DEFAULT '',
  PRIMARY KEY (`teamcode`),
  KEY `teamcode` (`teamcode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `subtitle` varchar(200) DEFAULT NULL,
  `content` text,
  `username` varchar(16) DEFAULT NULL,
  `ndate` date DEFAULT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `teaminfo`
--

DROP TABLE IF EXISTS `teaminfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teaminfo` (
  `teamcode` varchar(20) NOT NULL DEFAULT '',
  `mailinglist` text,
  `website` mediumtext,
  `notes` mediumtext,
  `status` int(2) DEFAULT NULL,
  `distributed` int(4) DEFAULT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `teamname` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`teamcode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `username` varchar(16) NOT NULL DEFAULT '',
  `password` varchar(50) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `teamcode` varchar(20) DEFAULT NULL,
  `comment` text,
  `last_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fullusername` varchar(64) DEFAULT NULL,
  `permissions` int(8) DEFAULT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-03-13  1:09:20
