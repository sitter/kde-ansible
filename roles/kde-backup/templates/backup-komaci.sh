#!/bin/bash
LOCATION=/root/{{backup_directory}}

# Cleanup old backups
find $LOCATION -mtime +3 | xargs rm -f

# Backup the installed software
tar -cJf $LOCATION/srv-bugs.`date +%w`.tar.xz -C / srv/

# Backup all our home directories
tar -cJf $LOCATION/homes.`date +%w`.tar.xz -C / home

# Grab the system config, package listing and cronjobs
dpkg -l > $LOCATION/dpkg.`date +%w`
tar -czf $LOCATION/crontabs.`date +%w`.tgz -C / var/spool/cron/
tar -czf $LOCATION/etc.`date +%w`.tar.gz -C / etc

chmod -R 700 $LOCATION

# Transfer them to the backup server
cd $LOCATION/..
lftp -f ~/bin/backup-options


# Prepare to run Borg backups
export BORG_PASSPHRASE='{{backup_borg_passphrase}}'

{% set backup_creds = hetzner_backup_creds[hetzner_backup_host] %}
BORG_SERVER="ssh://{{backup_creds.username}}@{{backup_creds.hostname}}:23"

# Backup databases into Borg
export BORG_REPO="$BORG_SERVER/./borg-backups/bugs-database"
mysqldump --opt --quick --single-transaction --skip-extended-insert --events --create-options --set-charset bugs | borg create --compression lzma,3 ::'{hostname}-db-backups-{now}' - 2>&1 | grep -v "Compacting segments"
borg prune --prefix '{hostname}-' --keep-daily 7 --keep-weekly 4 --keep-monthly 3 2>&1 | grep -v "Compacting segments"

