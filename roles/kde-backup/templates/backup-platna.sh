#!/bin/bash
LOCATION=/root/{{backup_directory}}

# Backup website contents
tar -cJf $LOCATION/srv.`date +%w`.tar.xz -C / srv/

# Backup configuration and list of installed packages
tar -czf $LOCATION/etc.`date +%w`.tgz -C / etc/
tar -czf $LOCATION/crontabs.`date +%w`.tgz -C / var/spool/cron/
dpkg -l > $LOCATION/dpkg.`date +%w`

# Transfer backups to backup server
cd $LOCATION/..
{% set backup_creds = hetzner_backup_creds[hetzner_backup_host] %}
rsync --timeout=600 --delete -a -e 'ssh -p23' {{backup_directory}}/ {{backup_creds.username}}@{{backup_creds.hostname}}:backups/

# Prepare to run Borg backups
export BORG_PASSPHRASE='{{backup_borg_passphrase}}'
BORG_SERVER="ssh://{{backup_creds.username}}@{{backup_creds.hostname}}:23"

# Backup Prosody
export BORG_REPO="$BORG_SERVER/./borg-backups/kdetalk-prosody"
borg create --compression zlib,5 --exclude-caches ::'{hostname}-kdetalk-prosody-{now}' /var/lib/prosody/ 2>&1 | grep -v "Compacting segments"
borg prune --prefix '{hostname}-' --keep-daily 7 --keep-weekly 4 --keep-monthly 6 2>&1 | grep -v "Compacting segments"

# Backup homes
export BORG_REPO="$BORG_SERVER/./borg-backups/homes"
borg create --compression zlib,5 --exclude-caches ::'{hostname}-homes-{now}' /home/ 2>&1 | grep -v "Compacting segments"
borg prune --prefix '{hostname}-' --keep-daily 7 --keep-weekly 4 --keep-monthly 6 2>&1 | grep -v "Compacting segments"
