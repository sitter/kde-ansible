#!/bin/bash
{% set backup_creds = hetzner_backup_creds[hetzner_backup_host] %}
LOCATION=/root/backups
find $LOCATION -name '*.tar' -mtime +7 | xargs rm -f

# Backup configuration and list of installed packages
tar -czf $LOCATION/etc.`date +%w`.tgz -C / etc/
tar -czf $LOCATION/crontabs.`date +%w`.tgz -C / var/spool/cron/
dpkg -l > $LOCATION/dpkg.`date +%w`

# Backup homes
tar -czf $LOCATION/homes.`date +%w`.tgz -C / home/

# Transfer the backups to the backup server
rsync -rav -e 'ssh -p 23' backups/ {{backup_creds.username}}@{{backup_creds.hostname}}:backups/

# Prepare to run Borg backups
export BORG_PASSPHRASE='{{backup_borg_passphrase}}'

BORG_SERVER="ssh://{{backup_creds.username}}@{{backup_creds.hostname}}:23"

# Backup our web applications
export BORG_REPO="$BORG_SERVER/./borg-backups/srv-www"
borg create --compression lzma,3 --exclude-caches ::'{hostname}-web-apps-{now}' /srv/www/ 2>&1 | grep -v "Compacting segments"
borg prune --prefix '{hostname}-' --keep-daily 7 --keep-weekly 4 --keep-monthly 6 2>&1 | grep -v "Compacting segments"

# Backup the bulk data stored by our web applications
export BORG_REPO="$BORG_SERVER/./borg-backups/srv-data"
borg create --compression lzma,3 --exclude-caches ::'{hostname}-web-data-{now}' /srv/data/ 2>&1 | grep -v "Compacting segments"
borg prune --prefix '{hostname}-' --keep-daily 7 --keep-weekly 4 --keep-monthly 6 2>&1 | grep -v "Compacting segments"

# Backup our databases
export BORG_REPO="$BORG_SERVER/./borg-backups/postgres-databases"
sudo -u postgres -H sh -c "cd /var/lib/postgresql/ && pg_dump collaborate" | borg create --compression lzma,3 "::{hostname}-collaborate-{now}" - 2>&1 | grep -v "Compacting segments"
sudo -u postgres -H sh -c "cd /var/lib/postgresql/ && pg_dump onlyoffice" | borg create --compression lzma,3 "::{hostname}-onlyoffice-{now}" - 2>&1 | grep -v "Compacting segments"
borg prune --prefix '{hostname}-collaborate' --keep-daily 7 --keep-weekly 4 --keep-monthly 6 2>&1 | grep -v "Compacting segments"
borg prune --prefix '{hostname}-onlyoffice' --keep-daily 7 --keep-weekly 4 --keep-monthly 6 2>&1 | grep -v "Compacting segments"
