# SPDX-FileCopyrightText: 2020 Nicolás Alvarez <nicolas.alvarez@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later

def parse_ldif(ldif_data):
    objects = []
    obj = {}

    lines = ldif_data.split('\n')

    for line in lines:
        line = line.strip()
        if line.startswith('#'): continue

        key, _, value = line.partition(':')
        value=value.lstrip()
        if line == '':
            obj = {}
        elif key == 'dn':
            obj = {'dn': value, 'attrs': {}, 'objectClass': [], 'name': value}
            objects.append(obj)
        elif key == 'objectClass':
            obj['objectClass'].append(value)
        else:
            obj['attrs'][key] = value

    return objects


class FilterModule:
    def filters(self):
        return {
            'parse_ldif': parse_ldif
        }
